# espNeoPixelRing8DemoDE

Dieses Repository beinhaltet ein kleines Beispiel-Programm um einen Neopixel Ring mit 8 Pixeln an einem ESP8266 zu zeigen.
Es ist für Anfänger gedacht. Daher wird auch beschrieben, welche Programme installiert werden müssen.

## Erste Installation

Damit du das Programm bearbeitet und auf den ESP8266 übertragen kannst, musst du zuerst deinen Computer dafür vorbereiten.

Installiere dir zuerst [Visual Studio Code](https://code.visualstudio.com/) (VSCode).

Dann benötigst du noch einen Git-Client.
[Hier gibt es eine Anleitung](https://git-scm.com/book/de/v2/Erste-Schritte-Git-installieren), wie du diesen installieren kannst.

Anschließend benötigst du die Erweiterung `PlatformIO` in VSCode.

1. Öffne die VSCode Paketverwaltung.
2. Suche nach der offiziellen `platformio ide` Erweiterung.
3. Installiere PlatformIO IDE.

Außerdem benötigst du bei einem Windows-Betriebssystem noch den passenden USB-Treiber.
Diesen kannst du dir [hier](https://github.com/nodemcu/nodemcu-devkit/tree/master/Drivers) herunterladen und installieren.

## Quelltext herunterladen

Weiter oben findest du die Möglichkeit das Repository als ZIP-Archiv herunterzuladen:

![Repo Download hint](doc/assets/download_repo.png)

Entpacke dieses in einem beliebigen Ordner.

*Alternativ:* Nutze den Git-Client um das Repository zu klonen. Mehr Infos dazu [hier](https://www.atlassian.com/de/git/tutorials/setting-up-a-repository/git-clone).

## Projekt in VSCode öffnen

Damit du an dem Projekt arbeiten und es auf den ESP8266 übertragen kannst, musst du dieses in VSCode öffnen.

Dafür gehst du im oberen Menü auf `Datei -> Ordner öffnen` und wählst den Ordner aus, den du im vorigen Schritt heruntergeladen und entpackt hast.

In der Datei `src/main.cpp` findest du das Programm.

## Programm übersetzen und übertragen

In der linken unteren Ecke von VSCode findest du die PlatformIO Toolbar.

![PlatformIO Toolbar](doc/assets/platformio_toolbar.png)

1. [PlatformIO Home](https://docs.platformio.org/en/latest/home/index.html#piohome)
2. Baue Programm
3. Lade Programm hoch
4. Räume Projekt auf
5. Serieller Port Monitor
6. [PlatformIO Code (CLI)](https://docs.platformio.org/en/latest/core/index.html#piocore)

Um das Programm auf den ESP8266 zu übertragen muss dieser natürlich per USB-Kabel an den PC angeschlossen sein.

## Weitere Informationen

- In [diesem Tutorial](https://devdrik.de/arduino-in-vs-code/) wird dir die Installation in deutsch noch etwas ausführlicher erklärt.
  
- Interessiert du dich wie du z.B. ein neues eigenen Projekt anlegen kannst, dann schau [hier](https://docs.platformio.org/en/latest/integration/ide/vscode.html) vorbei.

- In [diesem Tutorial](https://tttapa.github.io/ESP8266/Chap01%20-%20ESP8266.html) findest du einige hilfreiche Beispiele und Hinweise, was du mit einem ESP8266 alles tun kannst. (Das Kapitel *Software* kannst du natürlich überspringen. Dies haben wir schon hier erledigt.)
