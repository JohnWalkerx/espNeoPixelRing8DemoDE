#include <Adafruit_NeoPixel.h>

// Definiert den Ausgangs-Pin, an dem die LEDs angeschlossen werden.
static const int gc_outputPin = D4;
// Definiert die Anzahl der angeschlossenen LEDs.
static const int gc_pixelCount = 8;

Adafruit_NeoPixel pixels(gc_pixelCount, gc_outputPin, NEO_GRB + NEO_KHZ800);

// Definiert die Verzögerung in Millisekunden.
static const int gc_delayTime = 1000;

// ** Globale Merker Variabeln **
// Das als nächste anzuzeigende Pixel (0 bis 7)
static int g_nextShownPixel = 0;

// Zeit in Millisekunden der letzten Änderung
static int g_lastChange = 0;

void setup ()
{
    // Initialisiert die Kommunikation zu den LEDs
    pixels.begin();
}

void loop ()
{
    // Ist Zeit bereits abgelaufen? Wenn nein, dann probiere es beim nächsten Mal nochmal.
    if ((millis() - g_lastChange) <= gc_delayTime)
    {
        return;
    }

    // Setze letzte Änderungszeit.
    g_lastChange = millis();


    // Setzt alle LEDs zurück.
    pixels.clear();

    // Setze Pixel mit Nummer "nextShownPixel" auf definierte Farbe.
    // Farbe wird als RGB (Rot, Grün, Blau) angegeben.
    pixels.setPixelColor(g_nextShownPixel, pixels.Color(100, 0, 100));


    // Sendet die aktualisierten Pixel-Farben an die Hardware.
    pixels.show();


    // Erhöhe nächst zeigendes Pixel um 1.
    g_nextShownPixel++;

    // Stelle sicher, dass nach letztem Pixel (7) wieder von vorne begonnen wird.
    if (g_nextShownPixel >= gc_pixelCount)
    {
        g_nextShownPixel = 0;
    }
}
